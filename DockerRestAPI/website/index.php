<html>
    <head>
        <title>CIS 322 REST-api</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>

    <body>
        <h1>To See All Times:</h1>
        <p>Please go to http://localhost:5002/listall.php to see open, close, and both times in json and csv format</p>

        <h1>To See Top Times:</h1>
        <p>
            Please go to http://localhost:5002/listtoptimes.php and enter top times through the url to see top open and close times. 
            To specify the number of top times, use ?top=number as an url extension. For example. to see the top 3 open
            times, go to http://localhost:5002/listtoptimes.php?top=3
        </p>
        
    </body>
</html>
