<html>
    <head>
        <title>Top Times</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>

    <body>
        <h1>Top Open Times JSON</h1>
        <ul>
            <?php
            $arg = $_GET['top'];
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=' . $arg);
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li>Open: $t->open</li>";
            }
            ?>
        </ul>

        <h1>Top Close Times JSON</h1>
        <ul>
            <?php
            $arg = $_GET['top'];
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=' . $arg);
            $obj = json_decode($json);
              $times = $obj->result;
            foreach ($times as $t) {
                echo "<li>Close: $t->close</li>";
            }
            ?>
        </ul>

        <h1>Top Open Times CSV</h1>
        <ul>
            <?php
            $arg = $_GET['top'];
            $json = file_get_contents('http://laptop-service/listOpenOnly/json?top=' . $arg);
            $obj = json_decode($json);
              $times = $obj->result;
            echo "<li>open</li>";
            foreach ($times as $t) {
                echo "<li>$t->open</li>";
            }
            ?>
        </ul>

        <h1>Top Close Times CSV</h1>
        <ul>
            <?php
            $arg = $_GET['top'];
            $json = file_get_contents('http://laptop-service/listCloseOnly/json?top=' . $arg);
            $obj = json_decode($json);
              $times = $obj->result;
            echo "<li>close</li>";
            foreach ($times as $t) {
                echo "<li>$t->close</li>";
            }
            ?>
        </ul>

    </body>
</html>
