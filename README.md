# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database. Building on top of project 5 to implement an API to return the open and close times from the brevet calculator in JSON and csv format, as well as a consumer program that exposes the API.

## Functionality

* There are three hosts: 5000 for the calculator, 5001 for the API, and 5002 for the consumer program.

* To access the flask brevets calculator (from project 5), go to http://localhost:5000.

* To access the API, go to http://localhost:5001.

* To access the consumer program, go to http://localhost:5002. On this page, there are more specific instructions to follow to access the listall.php page as well as the listtoptimes.php, where the number is decided by the value added to the url, where http://localhost:5002/listtoptimes.php?top=3 would list the top 3 times.

*Both hosts, 5000 and 5001, use the services listed below.

    * "http://<host:port>/listAll" returns all open and close times in the database
    * "http://<host:port>/listOpenOnly" returns open times only
    * "http://<host:port>/listCloseOnly" returns close times only
    * "http://<host:port>/listAll/csv" returns all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" returns open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" returns close times only in CSV format
    * "http://<host:port>/listAll/json" returns all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" returns open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" returns close times only in JSON format

* Additionally, for listOpenOnly and listCloseOnly, you can tell the page how many to list by changing the last number in the url. Here are some examples:
    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format


## Author

Callista West
CIS 322
Fall 2020
